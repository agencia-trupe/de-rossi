<?php

use Illuminate\Database\Seeder;

class PerfilSeeder extends Seeder
{
    public function run()
    {
        DB::table('perfil')->insert([
            'chamada' => '',
            'texto_1' => '',
            'texto_2' => '',
            'imagem' => '',
        ]);
    }
}
