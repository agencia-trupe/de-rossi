<?php

use Illuminate\Database\Seeder;
use Illuminate\Database\Eloquent\Model;

class DatabaseSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Model::unguard();

        $this->call(UserTableSeeder::class);
        $this->call(PortfolioCapaSeeder::class);
		$this->call(ProjetosCapaSeeder::class);
		$this->call(PerfilSeeder::class);
        $this->call(ContatoTableSeeder::class);

        Model::reguard();
    }
}
