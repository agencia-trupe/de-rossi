<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePortfolioTable extends Migration
{
    public function up()
    {
        Schema::create('portfolio', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('ordem')->default(0);
            $table->string('titulo');
            $table->string('slug');
            $table->string('local');
            $table->string('ano');
            $table->string('parceria');
            $table->string('capa');
            $table->timestamps();
        });

        Schema::create('portfolio_imagens', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('portfolio_id')->unsigned();
            $table->integer('ordem')->default(0);
            $table->string('imagem');
            $table->timestamps();
            $table->foreign('portfolio_id')->references('id')->on('portfolio')->onDelete('cascade');
        });
    }

    public function down()
    {
        Schema::drop('portfolio_imagens');
        Schema::drop('portfolio');
    }
}
