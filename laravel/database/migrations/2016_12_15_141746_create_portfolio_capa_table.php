<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePortfolioCapaTable extends Migration
{
    public function up()
    {
        Schema::create('portfolio_capa', function (Blueprint $table) {
            $table->increments('id');
            $table->string('imagem');
            $table->timestamps();
        });
    }

    public function down()
    {
        Schema::drop('portfolio_capa');
    }
}
