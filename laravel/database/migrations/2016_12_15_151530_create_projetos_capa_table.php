<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateProjetosCapaTable extends Migration
{
    public function up()
    {
        Schema::create('projetos_capa', function (Blueprint $table) {
            $table->increments('id');
            $table->string('imagem');
            $table->timestamps();
        });
    }

    public function down()
    {
        Schema::drop('projetos_capa');
    }
}
