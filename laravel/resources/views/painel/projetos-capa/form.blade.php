@include('painel.common.flash')

<div class="well form-group">
    {!! Form::label('imagem', 'Imagem (1200x200px)') !!}
    <img src="{{ url('assets/img/projetos-capa/'.$registro->imagem) }}" style="display:block; margin-bottom: 10px; max-width: 100%;">
    {!! Form::file('imagem', ['class' => 'form-control']) !!}
</div>

{!! Form::submit($submitText, ['class' => 'btn btn-success']) !!}
