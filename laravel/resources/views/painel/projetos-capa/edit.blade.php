@extends('painel.common.template')

@section('content')

    <a href="{{ route('painel.projetos.index') }}" class="btn btn-sm btn-default">
        &larr; Voltar para Projetos
    </a>

    <legend>
        <h2><small>Projetos /</small> Editar Imagem de Capa</h2>
    </legend>

    {!! Form::model($registro, [
        'route'  => ['painel.projetos-capa.update', $registro->id],
        'method' => 'patch',
        'files'  => true])
    !!}

    @include('painel.projetos-capa.form', ['submitText' => 'Alterar'])

    {!! Form::close() !!}

@endsection
