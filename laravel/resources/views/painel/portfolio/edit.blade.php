@extends('painel.common.template')

@section('content')

    <legend>
        <h2><small>Portfolio /</small> Editar Portfolio</h2>
    </legend>

    {!! Form::model($registro, [
        'route'  => ['painel.portfolio.update', $registro->id],
        'method' => 'patch',
        'files'  => true])
    !!}

    @include('painel.portfolio.form', ['submitText' => 'Alterar'])

    {!! Form::close() !!}

@endsection
