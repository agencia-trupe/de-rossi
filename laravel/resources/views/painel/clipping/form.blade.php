@include('painel.common.flash')

<div class="form-group">
    {!! Form::label('titulo', 'Título') !!}
    {!! Form::text('titulo', null, ['class' => 'form-control']) !!}
</div>

<div class="well form-group">
    {!! Form::label('imagem', 'Imagem (opcional)') !!}
@if($submitText == 'Alterar' && $registro->imagem)
    <img src="{{ url('assets/img/clipping/'.$registro->imagem) }}" style="display:block; margin-bottom: 10px; max-width: 100%;">
    <a href="{{ route('painel.clipping.deleteImage', $registro->id) }}" class="btn-delete btn-delete-link label label-danger" style="display:inline-block;margin-bottom:10px">
        <i class="glyphicon glyphicon-remove" style="margin-right:5px"></i>
        REMOVER
    </a>
@endif
    {!! Form::file('imagem', ['class' => 'form-control']) !!}
</div>

<div class="well form-group">
    {!! Form::label('pdf', 'PDF (opcional, se preenchido precede a galeria de imagens)') !!}
    @if($submitText == 'Alterar' && $registro->pdf)
    <p>
        <a href="{{ url('assets/pdfs/'.$registro->pdf) }}" target="_blank" style="display:block;">{{ $registro->pdf }}</a>
        <a href="{{ route('painel.clipping.deletePdf', $registro->id) }}" class="btn-delete btn-delete-link label label-danger">
            <i class="glyphicon glyphicon-remove" style="margin-right:5px"></i>
            REMOVER
        </a>
    </p>
    @endif
    {!! Form::file('pdf', ['class' => 'form-control']) !!}
</div>

<div class="well form-group">
    {!! Form::label('link', 'Link (opcional, se preenchido precede a galeria de imagens e PDF)') !!}
    {!! Form::text('link', null, ['class' => 'form-control']) !!}
</div>

<div class="well" style="padding-bottom:5px">
    <label style="margin-bottom:14px">Vídeo (opcional, se preenchido precede a galeria de imagens, PDF e link)</label>
    <div class="row">
        <div class="col-md-6">
            <div class="form-group">
                {!! Form::label('video_tipo', 'Tipo') !!}
                {!! Form::select('video_tipo', ['youtube' => 'YouTube', 'vimeo' => 'Vimeo'], null, ['class' => 'form-control', 'placeholder' => 'Selecione']) !!}
            </div>
        </div>
        <div class="col-md-6">
            <div class="form-group">
                {!! Form::label('video_codigo', 'Código') !!}
                {!! Form::text('video_codigo', null, ['class' => 'form-control']) !!}
            </div>
        </div>
    </div>
</div>

{!! Form::submit($submitText, ['class' => 'btn btn-success']) !!}

<a href="{{ route('painel.clipping.index') }}" class="btn btn-default btn-voltar">Voltar</a>
