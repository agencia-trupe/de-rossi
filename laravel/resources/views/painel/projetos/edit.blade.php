@extends('painel.common.template')

@section('content')

    <legend>
        <h2><small>Apresentações /</small> Editar Apresentação</h2>
    </legend>

    {!! Form::model($registro, [
        'route'  => ['painel.projetos.update', $registro->id],
        'method' => 'patch',
        'files'  => true])
    !!}

    @include('painel.projetos.form', ['submitText' => 'Alterar'])

    {!! Form::close() !!}

@endsection
