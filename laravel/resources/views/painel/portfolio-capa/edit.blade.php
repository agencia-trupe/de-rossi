@extends('painel.common.template')

@section('content')

    <a href="{{ route('painel.portfolio.index') }}" class="btn btn-sm btn-default">
        &larr; Voltar para Portfolio
    </a>

    <legend>
        <h2><small>Portfolio /</small> Editar Imagem de Capa</h2>
    </legend>

    {!! Form::model($registro, [
        'route'  => ['painel.portfolio-capa.update', $registro->id],
        'method' => 'patch',
        'files'  => true])
    !!}

    @include('painel.portfolio-capa.form', ['submitText' => 'Alterar'])

    {!! Form::close() !!}

@endsection
