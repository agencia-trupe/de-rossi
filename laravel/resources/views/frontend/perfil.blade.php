@extends('frontend.common.template')

@section('content')

    <div class="main perfil">
        <div class="center">
            <h1>QUEM FAZ</h1>

            <div class="chamada">
                <p>{!! $perfil->chamada !!}</p>
                <div class="imagem">
                    <img src="{{ asset('assets/img/perfil/'.$perfil->imagem) }}" alt="">
                </div>
            </div>

            <div class="texto">
                <div class="col">{!! $perfil->texto_1 !!}</div>
                <div class="col">{!! $perfil->texto_2 !!}</div>
            </div>
        </div>
    </div>

@endsection
