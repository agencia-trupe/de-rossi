@extends('frontend.common.template')

@section('content')

    <div class="main portfolio-show">
        <div class="center">
            <h1>PORTFOLIO</h1>
            <div class="projeto-descricao">
                <p>
                    {{ $portfolio->titulo }}
                    <span>{{ $portfolio->local }} &middot; {{ $portfolio->ano }} @if($portfolio->parceria) &middot; {{ $portfolio->parceria }}@endif</span>
                </p>
            </div>
            <div class="imagens">
                @foreach($portfolio->imagens as $imagem)
                <img src="{{ asset('assets/img/portfolio/imagens/'.$imagem->imagem) }}" alt="">
                @endforeach
            </div>
            <div class="navegacao">
                <a href="{{ route('portfolio') }}" class="anterior"><span>MENU</span></a>
                <a href="#" class="subir"><span>SUBIR</span></a>
                <a href="{{ $anterior ? route('portfolio', $anterior->slug) : '#' }}" class="proximo @if(!$anterior) hidden @endif"><span>PRÓXIMO</span></a>
            </div>
        </div>
    </div>

@endsection
