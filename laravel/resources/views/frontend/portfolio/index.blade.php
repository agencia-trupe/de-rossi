@extends('frontend.common.template')

@section('content')

    <div class="main portfolio-index">
        <div class="center">
            <h1>PORTFOLIO</h1>
            <div class="box"></div>

            <div class="portfolio-thumbs">
                @foreach($portfolio as $projeto)
                    <a href="{{ route('portfolio', $projeto->slug) }}">
                        <img src="{{ asset('assets/img/portfolio/capa/'.$projeto->capa) }}" alt="">
                        <div class="overlay">
                            <span>{{ $projeto->titulo }}</span>
                        </div>
                    </a>
                @endforeach
            </div>
        </div>
    </div>

@endsection
