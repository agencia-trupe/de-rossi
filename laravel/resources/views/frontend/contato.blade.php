@extends('frontend.common.template')

@section('content')

    <div class="main contato">
        <div class="center">
            <h1>CONTATO</h1>

            <div class="box">
                <span>{{ $contato->telefone }}</span>
                <form action="" id="form-contato" method="POST">
                    <div class="col">
                        <input type="text" name="nome" id="nome" placeholder="nome" required>
                        <input type="email" name="email" id="email" placeholder="e-mail" required>
                        <input type="text" name="telefone" id="telefone" placeholder="telefone">
                    </div>
                    <textarea name="mensagem" id="mensagem" placeholder="mensagem" required></textarea>
                    <input type="submit" value="ENVIAR">
                    <div id="form-contato-response"></div>
                </form>
            </div>

            <p class="endereco">{{ $contato->endereco }}</p>

            <div class="mapa">
                {!! $contato->google_maps !!}
            </div>
        </div>
    </div>

@endsection
