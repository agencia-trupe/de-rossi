    <footer>
        <div class="center">
            <nav>
                <a href="{{ route('home') }}">HOME</a>
                <a href="{{ route('perfil') }}">PERFIL</a>
                <a href="{{ route('portfolio') }}">PORTFOLIO</a>
                <a href="{{ route('projetos') }}">APRESENTAÇÕES</a>
                <a href="{{ route('clipping') }}">CLIPPING</a>
                <a href="{{ route('contato') }}">CONTATO</a>
                @if($contato->facebook)
                <a href="{{ $contato->facebook }}" class="social facebook">facebook</a>
                @endif
                @if($contato->instagram)
                <a href="{{ $contato->instagram }}" class="social instagram">instagram</a>
                @endif
            </nav>

            <p class="contato">
                {{ $contato->endereco }}
                <span>{{ $contato->telefone }}</span>
            </p>

            <p class="copyright">
                © {{ date('Y') }} {{ config('site.name') }} - Todos os direitos reservados.
                <span>|</span>
                <a href="http://www.trupe.net" target="_blank">Criação de sites</a>:
                <a href="http://www.trupe.net" target="_blank">Trupe Agência Criativa</a>
            </p>
        </div>
    </footer>
