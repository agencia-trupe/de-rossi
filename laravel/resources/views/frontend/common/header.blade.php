    <header>
        <div class="center">
            <nav id="nav-desktop">
                @include('frontend.common._nav')
            </nav>

            <button id="mobile-toggle" type="button" role="button">
                <span class="lines"></span>
            </button>

            <span class="telefone">{{ $contato->telefone }}</span>
            <a href="{{ route('home') }}" class="marca">{{ config('site.name') }}</a>
        </div>

        <nav id="nav-mobile">
            @include('frontend.common._nav')
        </nav>
    </header>
