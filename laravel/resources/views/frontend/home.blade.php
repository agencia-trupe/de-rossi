@extends('frontend.common.template')

@section('content')

    <div class="banners">
        @foreach($banners as $banner)
        <div class="slide" style="background-image: url('{{ asset('assets/img/banners/'.$banner->imagem) }}')">
            <span>{{ $banner->frase }}</span>
        </div>
        @endforeach
        <div class="cycle-pager"></div>
    </div>

@endsection
