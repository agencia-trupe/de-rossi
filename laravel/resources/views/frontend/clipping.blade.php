@extends('frontend.common.template')

@section('content')

    <div class="main clipping">
        <div class="center">
            <h1>CLIPPING</h1>
            <div class="box"></div>

            <div class="clipping-thumbs">
                @foreach($clipping as $thumb)
                @if($thumb->video_codigo)
                <a href="{{ $thumb->video_tipo === 'youtube' ? 'https://youtube.com/embed/'.$thumb->video_codigo.'?autoplay=1' : 'https://player.vimeo.com/video/'.$thumb->video_codigo }}" class="video">
                    <div class="video-thumb" style="background-image:url('{{ asset('assets/img/videos/'.$thumb->video_capa) }}')"></div>
                </a>
                @elseif($thumb->link || $thumb->pdf)
                <a href="{{ $thumb->pdf ? asset('assets/pdfs/'.$thumb->pdf) : $thumb->link }}" class="{{ $thumb->pdf ? 'pdf' : 'link' }}" target="_blank">
                    @if($thumb->imagem)
                    <img src="{{ asset('assets/img/clipping/'.$thumb->imagem) }}" alt="">
                    @else
                    <span>{{ $thumb->titulo }}</span>
                    @endif
                </a>
                @else
                <a href="#" data-galeria="{{ $thumb->id }}" class="imagem thumb-imagem" target="_blank">
                    @if($thumb->imagem)
                    <img src="{{ asset('assets/img/clipping/'.$thumb->imagem) }}" alt="">
                    @else
                    <span>{{ $thumb->titulo }}</span>
                    @endif
                @endif
                @endforeach
            </div>
        </div>
    </div>

    <div class="hidden">
        @foreach($clipping as $registro)
            @foreach($registro->imagens as $imagem)
                <a href="{{ asset('assets/img/clipping/imagens/'.$imagem->imagem) }}" class="fancybox" rel="galeria-{{ $registro->id }}"></a>
            @endforeach
        @endforeach
    </div>

@endsection
