@extends('frontend.common.template')

@section('content')

    <div class="main projetos-index">
        <div class="center">
            <h1>APRESENTAÇÕES</h1>
            <div class="box"></div>

            <div class="projetos-thumbs">
                @foreach($projetos as $projeto)
                    <a href="{{ route('projetos', $projeto->slug) }}">
                        <img src="{{ asset('assets/img/projetos/capa/'.$projeto->capa) }}" alt="">
                        <div class="overlay">
                            <span>{{ $projeto->titulo }}</span>
                        </div>
                    </a>
                @endforeach
            </div>
        </div>
    </div>

@endsection
