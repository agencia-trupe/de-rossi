@extends('frontend.common.template')

@section('content')

    <div class="main projetos-show">
        <div class="center">
            <h1>APRESENTAÇÕES</h1>
            <div class="projeto-descricao">
                <p>
                    {{ $projeto->titulo }}
                    <span>{{ $projeto->ano }}</span>
                </p>
            </div>
            <div class="imagens">
                @foreach($projeto->imagens as $imagem)
                <img src="{{ asset('assets/img/projetos/imagens/'.$imagem->imagem) }}" alt="">
                @endforeach
            </div>
            <div class="navegacao">
                <a href="{{ route('projetos') }}" class="anterior"><span>MENU</span></a>
                <a href="#" class="subir"><span>SUBIR</span></a>
                <a href="{{ $anterior ? route('projetos', $anterior->slug) : '#' }}" class="proximo @if(!$anterior) hidden @endif"><span>PRÓXIMO</span></a>
            </div>
        </div>
    </div>

@endsection
