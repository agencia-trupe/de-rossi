(function(window, document, $, undefined) {
    'use strict';

    var App = {};

    App.mobileToggle = function() {
        var $handle = $('#mobile-toggle'),
            $nav    = $('#nav-mobile');

        $handle.on('click touchstart', function(event) {
            event.preventDefault();
            $nav.slideToggle();
            $handle.toggleClass('close');
        });
    };

    App.bannersHome = function() {
        var $wrapper = $('.banners');
        if (!$wrapper.length) return;

        $wrapper.cycle({
            slides: '>.slide',
            pagerTemplate: '<a href=#>{{slideNum}}</a>'
        });
    };

    App.envioContato = function(event) {
        event.preventDefault();

        var $form     = $(this),
            $response = $('#form-contato-response');

        $response.fadeOut('fast');

        $.ajax({
            type: "POST",
            url: $('base').attr('href') + '/contato',
            data: {
                nome: $('#nome').val(),
                email: $('#email').val(),
                telefone: $('#telefone').val(),
                mensagem: $('#mensagem').val(),
            },
            success: function(data) {
                $response.fadeOut().text(data.message).fadeIn('slow');
                $form[0].reset();
            },
            error: function(data) {
                $response.fadeOut().text('Preencha todos os campos corretamente').fadeIn('slow');
            },
            dataType: 'json'
        });
    };

    App.portfolioSubir = function() {
        $('.subir').click(function(event) {
            event.preventDefault();
            $('html, body').animate({ scrollTop: 0 }, 'slow');
        });
    };

    App.lightboxVideo = function() {
        $('.video').fancybox({
            padding: 0,
            type: 'iframe',
            width: 800,
            height: 450,
            aspectRatio: true
        });
    };

    App.thumbFancybox = function() {
        $('.fancybox').fancybox({
            padding: 0,
            maxHeight: '90%',
            maxWidth: '90%',
        });

        $('.thumb-imagem').click(function(e) {
            var el, id = $(this).data('galeria');
            if(id){
                el = $('.fancybox[rel=galeria-' + id + ']:eq(0)');
                e.preventDefault();
                el.click();
            }
        });
    };

    App.init = function() {
        this.mobileToggle();
        this.bannersHome();
        this.portfolioSubir();
        this.lightboxVideo();
        this.thumbFancybox();
        $('#form-contato').on('submit', this.envioContato);
    };

    $(document).ready(function() {
        App.init();

        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });
    });

}(window, document, jQuery));
