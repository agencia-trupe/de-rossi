<?php

namespace App\Providers;

use Illuminate\Routing\Router;
use Illuminate\Foundation\Support\Providers\RouteServiceProvider as ServiceProvider;

class RouteServiceProvider extends ServiceProvider
{
    /**
     * This namespace is applied to the controller routes in your routes file.
     *
     * In addition, it is set as the URL generator's root namespace.
     *
     * @var string
     */
    protected $namespace = 'App\Http\Controllers';

    /**
     * Define your route model bindings, pattern filters, etc.
     *
     * @param  \Illuminate\Routing\Router  $router
     * @return void
     */
    public function boot(Router $router)
    {
        $router->model('clipping', 'App\Models\Clipping');
        $router->model('projetos-capa', 'App\Models\ProjetosCapa');
        $router->model('projetos', 'App\Models\Projeto');
		$router->model('portfolio-capa', 'App\Models\PortfolioCapa');
		$router->model('portfolio', 'App\Models\Portfolio');
		$router->model('perfil', 'App\Models\Perfil');
		$router->model('banners', 'App\Models\Banner');
        $router->model('recebidos', 'App\Models\ContatoRecebido');
        $router->model('contato', 'App\Models\Contato');
        $router->model('usuarios', 'App\Models\User');

        $router->bind('imagens', function($id, $route, $model = null) {
            if ($route->hasParameter('portfolio')) {
                $model = \App\Models\PortfolioImagem::find($id);
            } elseif ($route->hasParameter('projetos')) {
                $model = \App\Models\ProjetoImagem::find($id);
            } elseif ($route->hasParameter('clipping')) {
                $model = \App\Models\ClippingImagem::find($id);
            }

            return $model ?: abort('404');
        });

        $router->bind('projeto_slug', function($value) {
            return \App\Models\Projeto::whereSlug($value)->first() ?: abort('404');
        });
        $router->bind('portfolio_slug', function($value) {
            return \App\Models\Portfolio::whereSlug($value)->first() ?: abort('404');
        });

        parent::boot($router);
    }

    /**
     * Define the routes for the application.
     *
     * @param  \Illuminate\Routing\Router  $router
     * @return void
     */
    public function map(Router $router)
    {
        $router->group(['namespace' => $this->namespace], function ($router) {
            require app_path('Http/routes.php');
        });
    }
}
