<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use App\Helpers\CropImage;

use Cviebrock\EloquentSluggable\SluggableInterface;
use Cviebrock\EloquentSluggable\SluggableTrait;

class Projeto extends Model implements SluggableInterface
{
    use SluggableTrait;

    protected $sluggable = [
        'build_from' => 'titulo',
        'save_to'    => 'slug',
        'on_update'  => true
    ];

    protected $table = 'projetos';

    protected $guarded = ['id'];

    public function scopeOrdenados($query)
    {
        return $query->orderBy('ordem', 'ASC')->orderBy('id', 'DESC');
    }

    public static function upload_capa()
    {
        return CropImage::make('capa', [
            [
                'width'  => 392,
                'height' => 260,
                'path'   => 'assets/img/projetos/capa/'
            ],
            [
                'width'  => 594,
                'height' => 394,
                'path'   => 'assets/img/projetos/capa-grande/'
            ]
        ]);
    }

    public function imagens()
    {
        return $this->hasMany('App\Models\ProjetoImagem', 'projeto_id')->ordenados();
    }
}
