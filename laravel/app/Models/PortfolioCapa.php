<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use App\Helpers\CropImage;

class PortfolioCapa extends Model
{
    protected $table = 'portfolio_capa';

    protected $guarded = ['id'];

    public static function upload_imagem()
    {
        return CropImage::make('imagem', [
            'width'  => 1200,
            'height' => 200,
            'path'   => 'assets/img/portfolio-capa/'
        ]);
    }

}
