<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class PortfolioImagem extends Model
{
    protected $table = 'portfolio_imagens';

    protected $guarded = ['id'];

    public function scopePortfolio($query, $id)
    {
        return $query->where('portfolio_id', $id);
    }

    public function scopeOrdenados($query)
    {
        return $query->orderBy('ordem', 'ASC')->orderBy('imagem', 'ASC');
    }
}
