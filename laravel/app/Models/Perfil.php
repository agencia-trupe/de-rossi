<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use App\Helpers\CropImage;

class Perfil extends Model
{
    protected $table = 'perfil';

    protected $guarded = ['id'];

    public static function upload_imagem()
    {
        return CropImage::make('imagem', [
            'width'  => null,
            'height' => 425,
            'upsize' => true,
            'path'   => 'assets/img/perfil/'
        ]);
    }

}
