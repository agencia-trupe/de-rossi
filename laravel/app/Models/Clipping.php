<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use App\Helpers\CropImage;

class Clipping extends Model
{
    protected $table = 'clipping';

    protected $guarded = ['id'];

    public function scopeOrdenados($query)
    {
        return $query->orderBy('ordem', 'ASC')->orderBy('id', 'DESC');
    }

    public static function upload_imagem()
    {
        return CropImage::make('imagem', [
            'width'  => 220,
            'height' => 220,
            'bg'     => true,
            'path'   => 'assets/img/clipping/'
        ]);
    }

    public static function uploadPdf() {
        $file = \Request::file('pdf');

        $path = 'assets/pdfs/';
        $name = str_slug(pathinfo($file->getClientOriginalName(), PATHINFO_FILENAME)).'_'.date('YmdHis').'.'.$file->getClientOriginalExtension();

        $file->move($path, $name);
        return $name;
    }

    public function imagens()
    {
        return $this->hasMany('App\Models\ClippingImagem', 'clipping_id')->ordenados();
    }
}
