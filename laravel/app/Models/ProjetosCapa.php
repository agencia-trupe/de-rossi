<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use App\Helpers\CropImage;

class ProjetosCapa extends Model
{
    protected $table = 'projetos_capa';

    protected $guarded = ['id'];

    public static function upload_imagem()
    {
        return CropImage::make('imagem', [
            'width'  => 1200,
            'height' => 200,
            'path'   => 'assets/img/projetos-capa/'
        ]);
    }

}
