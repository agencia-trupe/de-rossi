<?php

namespace App\Http\Requests;

use App\Http\Requests\Request;

class ProjetoRequest extends Request
{
    public function authorize()
    {
        return true;
    }

    public function rules()
    {
        $rules = [
            'titulo' => 'required',
            'slug' => '',
            'ano' => 'required',
            'capa' => 'required|image',
        ];

        if ($this->method() != 'POST') {
            $rules['capa'] = 'image';
        }

        return $rules;
    }
}
