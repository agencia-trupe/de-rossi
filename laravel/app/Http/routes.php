<?php

Route::group(['middleware' => ['web']], function () {
    Route::get('/', 'HomeController@index')->name('home');
    Route::get('perfil', 'PerfilController@index')->name('perfil');
    Route::get('portfolio/{portfolio_slug?}', 'PortfolioController@index')->name('portfolio');
    Route::get('apresentacoes/{projeto_slug?}', 'ProjetosController@index')->name('projetos');
    Route::get('clipping', 'ClippingController@index')->name('clipping');
    Route::get('contato', 'ContatoController@index')->name('contato');
    Route::post('contato', 'ContatoController@post')->name('contato.post');

    // Painel
    Route::group([
        'prefix'     => 'painel',
        'namespace'  => 'Painel',
        'middleware' => ['auth']
    ], function() {
        Route::get('/', 'PainelController@index')->name('painel');

        /* GENERATED ROUTES */
        Route::get('clipping/delete-image/{midia_id}', 'ClippingController@deleteImage')->name('painel.clipping.deleteImage');
        Route::get('midia/delete-pdf/{midia_id}', 'ClippingController@deletePdf')->name('painel.clipping.deletePdf');
        Route::resource('clipping', 'ClippingController');
        Route::get('clipping/{clipping}/imagens/clear', [
            'as'   => 'painel.clipping.imagens.clear',
            'uses' => 'ClippingImagensController@clear'
        ]);
        Route::resource('clipping.imagens', 'ClippingImagensController');

        Route::resource('projetos-capa', 'ProjetosCapaController', ['only' => ['index', 'update']]);
        Route::resource('projetos', 'ProjetosController');
        Route::get('projetos/{projetos}/imagens/clear', [
            'as'   => 'painel.projetos.imagens.clear',
            'uses' => 'ProjetosImagensController@clear'
        ]);
        Route::resource('projetos.imagens', 'ProjetosImagensController');

		Route::resource('portfolio-capa', 'PortfolioCapaController', ['only' => ['index', 'update']]);
		Route::resource('portfolio', 'PortfolioController');
        Route::get('portfolio/{portfolio}/imagens/clear', [
            'as'   => 'painel.portfolio.imagens.clear',
            'uses' => 'PortfolioImagensController@clear'
        ]);
        Route::resource('portfolio.imagens', 'PortfolioImagensController');

		Route::resource('perfil', 'PerfilController', ['only' => ['index', 'update']]);
		Route::resource('banners', 'BannersController');
        Route::resource('contato/recebidos', 'ContatosRecebidosController');
        Route::resource('contato', 'ContatoController');
        Route::resource('usuarios', 'UsuariosController');

        Route::post('order', 'PainelController@order');
        Route::get('logs', '\Rap2hpoutre\LaravelLogViewer\LogViewerController@index');
    });

    // Auth
    Route::group([
        'prefix'    => 'painel',
        'namespace' => 'Auth'
    ], function() {
        Route::get('login', 'AuthController@showLoginForm')->name('auth');
        Route::post('login', 'AuthController@login')->name('login');
        Route::get('logout', 'AuthController@logout')->name('logout');
    });
});
