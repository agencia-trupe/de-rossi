<?php

namespace App\Http\Controllers\Painel;

use Illuminate\Http\Request;

use App\Http\Requests\ProjetosCapaRequest;
use App\Http\Controllers\Controller;

use App\Models\ProjetosCapa;

class ProjetosCapaController extends Controller
{
    public function index()
    {
        $registro = ProjetosCapa::first();

        return view('painel.projetos-capa.edit', compact('registro'));
    }

    public function update(ProjetosCapaRequest $request, ProjetosCapa $registro)
    {
        try {
            $input = $request->all();

            if (isset($input['imagem'])) $input['imagem'] = ProjetosCapa::upload_imagem();

            $registro->update($input);

            return redirect()->route('painel.projetos-capa.index')->with('success', 'Registro alterado com sucesso.');
        } catch (\Exception $e) {
            return back()->withErrors(['Erro ao alterar registro: '.$e->getMessage()]);
        }
    }
}
