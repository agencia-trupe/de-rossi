<?php

namespace App\Http\Controllers\Painel;

use Illuminate\Http\Request;

use App\Http\Requests\PortfolioCapaRequest;
use App\Http\Controllers\Controller;

use App\Models\PortfolioCapa;

class PortfolioCapaController extends Controller
{
    public function index()
    {
        $registro = PortfolioCapa::first();

        return view('painel.portfolio-capa.edit', compact('registro'));
    }

    public function update(PortfolioCapaRequest $request, PortfolioCapa $registro)
    {
        try {
            $input = $request->all();

            if (isset($input['imagem'])) $input['imagem'] = PortfolioCapa::upload_imagem();

            $registro->update($input);

            return redirect()->route('painel.portfolio-capa.index')->with('success', 'Registro alterado com sucesso.');
        } catch (\Exception $e) {
            return back()->withErrors(['Erro ao alterar registro: '.$e->getMessage()]);
        }
    }
}
