<?php

namespace App\Http\Controllers\Painel;

use Illuminate\Http\Request;

use App\Http\Requests\PortfolioRequest;
use App\Http\Controllers\Controller;

use App\Models\Portfolio;

class PortfolioController extends Controller
{
    public function index()
    {
        $registros = Portfolio::ordenados()->get();

        return view('painel.portfolio.index', compact('registros'));
    }

    public function create()
    {
        return view('painel.portfolio.create');
    }

    public function store(PortfolioRequest $request)
    {
        try {

            $input = $request->all();

            if (isset($input['capa'])) $input['capa'] = Portfolio::upload_capa();

            Portfolio::create($input);
            return redirect()->route('painel.portfolio.index')->with('success', 'Registro adicionado com sucesso.');

        } catch (\Exception $e) {

            return back()->withErrors(['Erro ao adicionar registro: '.$e->getMessage()]);

        }
    }

    public function edit(Portfolio $registro)
    {
        return view('painel.portfolio.edit', compact('registro'));
    }

    public function update(PortfolioRequest $request, Portfolio $registro)
    {
        try {

            $input = $request->all();

            if (isset($input['capa'])) $input['capa'] = Portfolio::upload_capa();

            $registro->update($input);
            return redirect()->route('painel.portfolio.index')->with('success', 'Registro alterado com sucesso.');

        } catch (\Exception $e) {

            return back()->withErrors(['Erro ao alterar registro: '.$e->getMessage()]);

        }
    }

    public function destroy(Portfolio $registro)
    {
        try {

            $registro->delete();
            return redirect()->route('painel.portfolio.index')->with('success', 'Registro excluído com sucesso.');

        } catch (\Exception $e) {

            return back()->withErrors(['Erro ao excluir registro: '.$e->getMessage()]);

        }
    }

}
