<?php

namespace App\Http\Controllers\Painel;

use Illuminate\Http\Request;

use App\Http\Requests\ClippingImagensRequest;
use App\Http\Controllers\Controller;

use App\Models\Clipping;
use App\Models\ClippingImagem;
use App\Helpers\CropImage;

class ClippingImagensController extends Controller
{
    private $image_config = [
        [
            'width'   => 360,
            'height'  => 180,
            'path'    => 'assets/img/clipping/imagens/thumbs/'
        ],
        [
            'width'   => 940,
            'height'  => null,
            'upsize'  => true,
            'path'    => 'assets/img/clipping/imagens/'
        ]
    ];

    public function index(Clipping $clipping)
    {
        $imagens = ClippingImagem::clipping($clipping->id)->ordenados()->get();

        return view('painel.clipping.imagens.index', compact('imagens', 'clipping'));
    }

    public function show(Clipping $clipping, ClippingImagem $imagem)
    {
        return $imagem;
    }

    public function create(Clipping $clipping)
    {
        return view('painel.clipping.imagens.create', compact('clipping'));
    }

    public function store(Clipping $clipping, ClippingImagensRequest $request)
    {
        try {

            $input = $request->all();
            $input['imagem'] = CropImage::make('imagem', $this->image_config);

            $imagem = $clipping->imagens()->create($input);

            $view = view('painel.clipping.imagens.imagem', compact('clipping', 'imagem'))->render();

            return response()->json(['body' => $view]);

        } catch (\Exception $e) {

            return 'Erro ao adicionar imagem: '.$e->getMessage();

        }
    }

    public function destroy(Clipping $clipping, ClippingImagem $imagem)
    {
        try {

            $imagem->delete();
            return redirect()->route('painel.clipping.imagens.index', $clipping)
                             ->with('success', 'Imagem excluída com sucesso.');

        } catch (\Exception $e) {

            return back()->withErrors(['Erro ao excluir imagem: '.$e->getMessage()]);

        }
    }

    public function clear(Clipping $clipping)
    {
        try {

            $clipping->imagens()->delete();
            return redirect()->route('painel.clipping.imagens.index', $clipping)
                             ->with('success', 'Imagens excluídas com sucesso.');

        } catch (\Exception $e) {

            return back()->withErrors(['Erro ao excluir imagens: '.$e->getMessage()]);

        }
    }
}
