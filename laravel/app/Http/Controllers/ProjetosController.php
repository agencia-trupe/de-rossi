<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;

use App\Models\Projeto;
use App\Models\ProjetosCapa;

class ProjetosController extends Controller
{
    public function index(Projeto $projeto)
    {
        if ($projeto->exists) {
            $anterior = Projeto::select('slug')
                            ->where('ordem', '<', $projeto->ordem)
                            ->orWhere(function($query) use ($projeto) {
                                $query->where('ordem', '=', $projeto->ordem)
                                      ->where('id', '<', $projeto->id);
                            })
                            ->orderBy('ordem', 'DESC')
                            ->orderBy('id', 'DESC')
                            ->first();

            $proximo  = Projeto::select('slug')
                            ->where('ordem', '>', $projeto->ordem)
                            ->orWhere(function($query) use ($projeto) {
                                $query->where('ordem', '=', $projeto->ordem)
                                      ->where('id', '>', $projeto->id);
                            })
                            ->orderBy('ordem', 'ASC')
                            ->orderBy('id', 'ASC')
                            ->first();

            return view('frontend.projetos.show', compact('projeto', 'anterior', 'proximo'));
        }

        return view('frontend.projetos.index')->with([
            'capa' => ProjetosCapa::first(),
            'projetos' => Projeto::ordenados()->get()
        ]);
    }
}
