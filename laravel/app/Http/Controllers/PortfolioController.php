<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;

use App\Models\Portfolio;
use App\Models\PortfolioCapa;

class PortfolioController extends Controller
{
    public function index(Portfolio $portfolio)
    {
        if ($portfolio->exists) {
            $anterior = Portfolio::select('slug')
                            ->where('ordem', '<', $portfolio->ordem)
                            ->orWhere(function($query) use ($portfolio) {
                                $query->where('ordem', '=', $portfolio->ordem)
                                      ->where('id', '<', $portfolio->id);
                            })
                            ->orderBy('ordem', 'DESC')
                            ->orderBy('id', 'DESC')
                            ->first();

            $proximo  = Portfolio::select('slug')
                            ->where('ordem', '>', $portfolio->ordem)
                            ->orWhere(function($query) use ($portfolio) {
                                $query->where('ordem', '=', $portfolio->ordem)
                                      ->where('id', '>', $portfolio->id);
                            })
                            ->orderBy('ordem', 'ASC')
                            ->orderBy('id', 'ASC')
                            ->first();

            return view('frontend.portfolio.show', compact('portfolio', 'anterior', 'proximo'));
        }

        return view('frontend.portfolio.index')->with([
            'capa' => PortfolioCapa::first(),
            'portfolio' => Portfolio::ordenados()->get()
        ]);
    }
}
