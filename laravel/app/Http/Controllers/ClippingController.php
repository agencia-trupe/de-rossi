<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;

use App\Models\Clipping;

class ClippingController extends Controller
{
    public function index()
    {
        return view('frontend.clipping')->with([
            'clipping' => Clipping::ordenados()->get()
        ]);
    }
}
